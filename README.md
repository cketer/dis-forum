Instructions:

The application is made up of the client-side which is the Vue.js front-end nad the server which is the Node.js API
Prerequisites

Install NodeJs -- https://nodejs.org/en/download/
Install VueJs -- https://vuejs.org/v2/guide/installation.html#NPM

How to run the project:
1. on the /client directory intall all dependencies using npm install
2. To run the app on a development server use 'npm run serve' command
3. Build the application so that Nodejs can use the build file as the front end, type in 'npm build'
    //Running the Node API
4. Move to the /server directory, and install all dependencies using 'npm install'
5. To run the app, use 'npm start'
6. The application will now successfully run on your localhost port:3000
