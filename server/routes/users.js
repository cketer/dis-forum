var express = require('express');
var router = express.Router();
// Place holder representation of JSON data from mongodb. users is the collection with three records Collins, Voan and Prem
var users = {
  "users":[
      {'name': 'Collins Keter'  },
      { 'name': 'Virakvoan Nget'},
      {'name':'Prem Gangadharan'}
]
};
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send({ users });
  // res.render(HelloWorld );
});

// router.get('/users', function(req, res, next) {
//   res.send({ users });
// });

module.exports = router;
